import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// import components here
import { HomeComponent } from './home/home.component';



// add paths here
const routes: Routes = [
  {path: '', redirectTo: '/home/1', pathMatch: 'full'},
  {path: 'home/:page', component: HomeComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }

export const routingComponents = [
  HomeComponent
]
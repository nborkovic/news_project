export interface IHash {
    [indexer: number] : string;
}

export class Comment {

    constructor(public author : string,
                public article_id : string,
                public text: string){
    }
}

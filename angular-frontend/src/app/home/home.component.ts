import { Component, OnInit } from "@angular/core";
import { NewsService } from "../services/news.service";

import { ActivatedRoute, Router } from "@angular/router";
import { Console } from "@angular/core/src/console";
import { IHash } from "./IHash";
import { Comment } from "./Comment";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.css"]
})
export class HomeComponent implements OnInit {
  page = null;
  news = null;
  errorMessage = null;

  //comments
  commentHash: IHash = {};
  comment = new Comment(null, null, null);
  status = null;

  constructor(
    private _newsService: NewsService,
    public _route: ActivatedRoute,
    public _router: Router
  ) {
    this._route.params.subscribe(params => (this.page = Number(params.page)));
  }

  ngOnInit() {
    console.log("[INFO] Getting the newest articles");
    this.commentHash = {};
    this.comment = new Comment(null, null, null);
    this.status = null;
    this._newsService.getArticles(this.page).subscribe(
      data => {
        console.log("[INFO] Storing data");
        this.news = data;
      },
      error => {
        console.log("[ERROR] " + error);
        this.errorMessage = error;
      }
    );
  }

  next() {
    var nextPage = parseInt(this.page) + 1;
    window.location.href = "http://localhost:4200/home/" + nextPage;
  }

  previous() {
    var previousPage;
    if (this.page > 1) {
      previousPage = parseInt(this.page) - 1;
    } else {
      previousPage = parseInt(this.page);
    }
    window.location.href = "http://localhost:4200/home/" + previousPage;
  }

  submitComment(article_id: string) {
    this.comment = new Comment(null, article_id, this.commentHash[article_id]);
    this._newsService.postComment(this.comment).subscribe(
      data => {
        console.log(data);
        this.ngOnInit();
      },
      error => {
        console.log(error);
      }
    );
    console.log(this.comment);
  }
}

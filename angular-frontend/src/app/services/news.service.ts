import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Comment } from '../home/Comment';

@Injectable({
  providedIn: 'root'
})
export class NewsService {

  constructor(private http: HttpClient) { }

  baseUrl = "http://localhost:5000";



  getArticles(page: number) {
    console.log("[INFO] Article service active for page "+ page);
    return this.http.get(`${this.baseUrl}/api/search/articles/${page}`);
  }

  postComment(comment: Comment){
    console.log("[INFO] Posting comment service active for comment :" + comment);
    return this.http.post(`${this.baseUrl}/api/search/comment/new`, comment ,{responseType : 'text'});
  }



}

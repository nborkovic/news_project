const express = require("express");
var PropertiesReader = require("properties-reader");
var properties = PropertiesReader("./application.properties");
const app = express();
const PORT = process.env.PORT || properties.get("server.port");
const CONNECTION_STRING = properties.get("connectionString");
const cors = require('cors');
const MongoClient = require('mongodb').MongoClient



// body parser
const bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({ limit: "10mb", extended: true }));
app.use(bodyParser.json({ limit: "10mb", extended: true }));

// CORS middleware
app.use(cors());


// api
const search = require ("./services/search");
app.use("/api/search", search);



MongoClient.connect(CONNECTION_STRING, (err, client) => {
  if (err) {
    console.log("[ERROR]   "+ err);
  } else {
    console.log("[SUCCESS] Connected to database!");
    db = client.db('nmbp');

    db.collection('articles').find().toArray( (err, results) => {
      console.log("[INFO]    Number of articles in database :" + results.length);
    });
  }
});

// run server
app.listen(PORT, () => {
  console.log(`[SUCCESS] Server is running on PORT: ${PORT}`);
});

const express = require("express");
const router = express.Router();

var PropertiesReader = require("properties-reader");
var properties = PropertiesReader("./application.properties");
const CONNECTION_STRING = properties.get("connectionString");
const MongoClient = require('mongodb').MongoClient;
const ObjectID = require('mongodb').ObjectID;

const cors = require("cors");


// get articles service
router.get("/articles/:page", cors(), function(req, res) {
  console.log("\n\n[INFO]    Articles search service active")
  var page = req.params.page;
  const articlesPerPage = 15;

  var skipArticles = (page - 1) * articlesPerPage


  MongoClient.connect(CONNECTION_STRING, (err, client) => {
    if (err) {
      console.log("[ERROR]   "+ err);
    } else {
      console.log("[SUCCESS] Connected to database!");
      db = client.db('nmbp');

      console.log("[INFO]    Skipping " + skipArticles + " getting max " + articlesPerPage + " articles");
      db.collection('articles').find().sort( {date: -1} ).skip(skipArticles).limit(articlesPerPage).toArray( (err, results) => {
        console.log("[INFO]    " + results.length + " results found");
        return res.status(200).json(results);
      });
    }
  });
});


router.post("/article/new", cors(), function(req, res) {
  console.log("\n\n[INFO]    Article creation service active")
  var newArticle = req.body;

  MongoClient.connect(CONNECTION_STRING, (err, client) => {
    if (err) {
      console.log("[ERROR]   "+ err);
    } else {
      console.log("[SUCCESS] Connected to database!");
      db = client.db('nmbp');

      db.collection('articles').insert({ title: newArticle.title, text: newArticle.text, author: newArticle.author, date: new Date(), image: newArticle.image, comments: [] });
   
    }
  });

  return res.status(200).json("ok");
});


router.post("/comment/new", cors(), function(req, res) {
  console.log("\n\n[INFO]    Comment creation service active for article: " + req.body.article_id);
  var newComment = req.body;

  var author;
  if (newComment.author){
    author = newComment.author;
  } else {
    author = "<Anonymous>"
  }
  

  MongoClient.connect(CONNECTION_STRING, (err, client) => {
    if (err) {
      console.log("[ERROR]   "+ err);
    } else {
      console.log("[SUCCESS] Connected to database!");
      db = client.db('nmbp');

      db.collection('articles').updateOne( {_id: new ObjectID(newComment.article_id)}, { $push : {comments :  {author : author, text: newComment.text, date: new Date() } } } );
    }
  });
  return res.status(200).json("ok");
});

module.exports = router;

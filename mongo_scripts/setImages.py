import base64
import requests
import pymongo
import json


def get_as_base64(url):
    return base64.b64encode(requests.get(url).content).decode()


client = pymongo.MongoClient("mongodb://localhost:27017/")
db = client["nmbp"]
collection = db["articles"]


articles_cursor = collection.find()

print (str(articles_cursor.count()) + " articles found")

counter = 0
for x in articles_cursor:
    counter += 1
    print("Update article ["+str(counter) + " of " +
          str(articles_cursor.count()) + "]")
    x.update({'image': get_as_base64('https://source.unsplash.com/300x200')})
 #   print(x['image'])
    collection.save(x)
 #   print ("\n\n")

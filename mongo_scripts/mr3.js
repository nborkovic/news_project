// MapReduce upit koji za svakog autora vraća prvih 10 najkorištenijih riječi.
// Pojam „riječ“ shvatiti u najjednostavnijem mogućem obliku (niz slova odvojen od drugih s razmakom,
// zarezom ili točkom).
// Nije potrebno raditi nikakve leksičke transformacije na riječima (svođene na korijen i sl.).
// Nije potrebno ispisati i 11. riječ ako se ima isti broj korištenja kao 10. riječ.


var map = function() {

	var str = this.text;
	var splitText = str.split(" ");
	var key = this.author;
	for (i = 0; i < splitText.length; ++i){
		emit (key, {key: key, freq: {word: splitText[i], count: 1}} );
	};
};


var reduce = function(key, values) {
	var result = {key: key, freq: {}};
	values.forEach( function (value) {
		if (result.freq.hasOwnProperty(value.freq.word)){
			result.freq[ value.freq.word ] += value.freq.count; 
		} else {
			result.freq[ value.freq.word ] = value.freq.count; 
		};		
	});
	return result;
}; 

var finalize = function (key, result) {

	var result_as_array = Object.keys(result.freq).map(function(key) {
  		return {word: key, count: result.freq[key]};
	});
	var sorted_results = result_as_array.sort((a,b) => (a.count > b.count) ? -1 : ((b.count > a.count) ? 1 : 0)); 
	var top_10 = sorted_results.slice(0,10);
	return top_10;
};



db.articles.mapReduce (map, reduce, {out: 'mr3_out', finalize: finalize});





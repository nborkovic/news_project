// MapReduce upit koji vraća broj članaka po broju komentara.

var mapFunc = function () {
	emit (this.comments.length, 1);
}

var reduceFunc = function (numComments, values) {
	count = 0;
	for (i=0; i < values.length; ++i) {
		count += values [i];
	}
	return count;
}
 
db.articles.mapReduce (mapFunc, reduceFunc, {out: "mr1_out"});

db.mr_out.find().pretty();
//set all to null
db.articles.updateMany({ }, { $set: {date: null }})


function getRandomInt(min, max) {
    return Math.floor(Math.random()* (max - min + 1)) + min;
}
    

function getRandomDate() {
    // random miliseconds between 2000 and 2020
    var miliseconds = getRandomInt(30*365*24*60*60*1000, 50*365*24*60*60*1000); 
    return new Date(miliseconds);
}
    

// for each article in database set new random date
db.articles.find().forEach( function (article) {db.articles.update ({_id: article._id}, {$set : {date : getRandomDate()}})})


// for each article add empty comments array

> db.articles.updateMany( { }, {$set: {comments : []}})



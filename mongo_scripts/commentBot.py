import pymongo
import json
import random
import datetime



# 20% articles will be comented
def shouldICmment(percent=20):
    return random.randrange(100) < percent


client = pymongo.MongoClient("mongodb://localhost:27017/")
db = client["nmbp"]
collection = db["articles"]


articles_cursor = collection.find()


print(str(articles_cursor.count()) + " articles found")

articlesCounter = 0
commentCounter = 0
counter = 0
for x in articles_cursor:
    counter += 1

    print("Update article ["+str(counter) + " of " +
          str(articles_cursor.count()) + "]")
    
    if not shouldICmment():
        continue

    # generate a comments
    newCommentLIst = x['comments']
    numberOfNewComments =  random.randrange(1,5,1)
    for i in range(numberOfNewComments):
        generated_comment = {}
        generated_comment['author'] = "<Comment_Bot>"
        generated_comment['text'] = "Hi, I am comment bot, Have a nice day"
        generated_comment['date'] = datetime.datetime.now()    
        commentCounter += 1    
        newCommentLIst.append(generated_comment)
        
    #update current article (dict)    
    commentDict = {'comments': newCommentLIst}
    x.update(commentDict)
    
    #save to collection
    articlesCounter +=1
    collection.save(x)
print (str(articlesCounter) + " articles commented with total "+str(commentCounter) +".")

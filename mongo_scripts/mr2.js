// MapReduce upit postotak komentiranih članaka (nije bitno koliko komentara imaju, već
// samo imaju li ili nemaju)

var mapFunction = function () { 
	if (this.comments.length > 0) { 
		emit ('group', {group: 'group', count: 1, sum : 1}); 
	} else {
		emit ('group', {group: 'group', count: 1, sum : 0}); 
	}
}


var reduceFunction = function (key, values) { 
	var results = {group: 'group', count: 0, sum : 0}; 

	values.forEach( function(value) {
		results.sum += value.sum;
		results.count += value.count;
	});

	return results;
}


var finalizeFunction = function (key, value) {
	var result = value.sum/value.count * 100 +" %"
	return { percentage : result}
}

db.articles.mapReduce (mapFunction, reduceFunction, {out: "mr2_out", finalize: finalizeFunction});


/************************************************************************************************/
 
// Napraviti web portal koji ispisuje najnovijih N (npr. N=10) vijesti.
// Omogućiti komentiranje vijesti: ispod svake vijesti dodati polje za unos komentara.
// Nije potrebno napraviti sučelje za unos vijesti, odnosno možete ih sve unijeti ručno.
// U bazi treba biti barem 10.000 vijesti. Možete ih generirati ili skinuti neki skup podataka s
// Interneta, ali svakako trebate (strojno) napuniti barem 10 tisuća vijesti.


// Baza podataka:
// Generirana automatski pomoću određenih internetskih mocking servisa 
// Dodatno uređena pomoću python skripti
// oblik dokumenta:
{
	"_id":{"$oid":"..."},
	"title":"...",
	"text":"...",
	"author":"...",
	"date":{"$date":"..."},
	"image":"base64image",
	"comments":[ {	author:"...", 
			text:"...", 
			date":{"$date":"..."}
		      } ]
}
// slučajne slike su pomoću python skripte dohvaćene sa interneta kodirane u base64 formatu
// zatim spremljene u bazu podataka

// Backend:
// Node.js + express server


//Frontend:
// Angular2+



/************************************************************************************************/
// MapReduce upit koji vraća broj članaka po broju komentara.

var mapFunc = function () {
	emit (this.comments.length, 1);
}

var reduceFunc = function (numComments, values) {
	count = 0;
	for (i=0; i < values.length; ++i) {
		count += values [i];
	}
	return count;
}
 
db.articles.mapReduce (mapFunc, reduceFunc, {out: "mr_out"});






/************************************************************************************************/
// MapReduce upit postotak komentiranih članaka (nije bitno koliko komentara imaju, već
// samo imaju li ili nemaju)

var mapFunction = function () { 
	if (this.comments.length > 0) { 
		emit ('group', {group: 'group', count: 1, sum : 1}); 
	} else {
		emit ('group', {group: 'group', count: 1, sum : 0}); 
	}
}


var reduceFunction = function (key, values) { 
	var results = {group: 'group', count: 0, sum : 0}; 

	values.forEach( function(value) {
		results.sum += value.sum;
		results.count += value.count;
	});

	return results;
}


var finalizeFunction = function (key, value) {
	var result = value.sum/value.count * 100 +" %"
	return { percentage : result}
}

db.articles.mapReduce (mapFunction, reduceFunction, {out: "mr_out", finalize: finalizeFunction});


/************************************************************************************************/
// MapReduce upit koji za svakog autora vraća prvih 10 najkorištenijih riječi.
// Pojam „riječ“ shvatiti u najjednostavnijem mogućem obliku (niz slova odvojen od drugih s razmakom,
// zarezom ili točkom).
// Nije potrebno raditi nikakve leksičke transformacije na riječima (svođene na korijen i sl.).
// Nije potrebno ispisati i 11. riječ ako se ima isti broj korištenja kao 10. riječ.


var map = function() {

	var str = this.text;
	var splitText = str.split(" ");
	var key = this.author;
	for (i = 0; i < splitText.length; ++i){
		emit (key, {key: key, freq: {word: splitText[i], count: 1}} );
	};
};


var reduce = function(key, values) {
	var result = {key: key, freq: {}};
	values.forEach( function (value) {
		if (result.freq.hasOwnProperty(value.freq.word)){
			result.freq[ value.freq.word ] += value.freq.count; 
		} else {
			result.freq[ value.freq.word ] = value.freq.count; 
		};		
	});
	return result;
}; 

var finalize = function (key, result) {

	var result_as_array = Object.keys(result.freq).map(function(key) {
  		return {word: key, count: result.freq[key]};
	});
	var sorted_results = result_as_array.sort((a,b) => (a.count > b.count) ? -1 : ((b.count > a.count) ? 1 : 0)); 
	var top_10 = sorted_results.slice(0,10);
	return top_10;
};



db.articles.mapReduce (map, reduce, {out: 'mr_out', finalize: finalize});




